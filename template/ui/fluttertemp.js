
//------------------- flutter模板 模块 -------------------

function tmpl_flutter_xml() {
    var string =
        "   Container(\n" +
        "        width: 1.sw,\n" +
        "        height: 1.sh,\n" +
        "        {% if (o.root.bgColor) { %}color: const Color(0xFFF0F4F7),{% } %}\n" +
        "        {% if (o.root.views) { %}\n" +
        "         {% var templateViewsValue = tmpl_flutter_views();var map = {};map[\"root\"] = o.root.views; var ret = tmpl(templateViewsValue, map); %}\n" +
        "                      {% if(ret) { %}\n" +
        "                      child: Stack(\n" +
        "         fit: StackFit.passthrough,\n" +
        "         children: <Widget>[\n" +
        "         {%=ret%}\n" +
        "         ]\n" +
        "         ),\n" +
        "         {% } %}\n" +
        "         {% } %}\n" +
        "   ),";
    return string;
}

function tmpl_flutter_views() {
    var string =
    "{% for (var i=0; i<o.root.length; i++) { %}\n" +
    "          {% var viewInfo = o.root[i]; %}\n" +
    "\n" +
    "          {% if (viewInfo.viewType == \"text\") { %}\n" +
    "    Positioned(\n" +
    "     left: {%=viewInfo.x %}.r,\n" +
    "     top: {%=viewInfo.y %}.r,\n" +
    "     child: Text(\n" +
    "     {% if (viewInfo.text) { %} {%=viewInfo.text %} {% } %},\n" +
    "       textAlign: TextAlign.{%=viewInfo.align %},\n" +
    "       overflow: TextOverflow.visible,\n" +
    "       softWrap: false,\n" +
    "       style: TextStyle(\n" +
    "        {% if (viewInfo.text) { %} fontFamily: \"{%=viewInfo.font_family %}\", {% } %}\n" +
    "           fontSize: {% if (viewInfo.font) { %}{%=viewInfo.font %}.sp{% } else { %}14.sp{% } %},\n" +
    "           color: Color({%=viewInfo.textColor %}),\n" +
    "       ),\n" +
    "      )\n" +
    "    ),\n" +
    "    {% } %}\n" +
    "\n" +
    "          {% if (viewInfo.viewType == \"textField\") { %}\n" +
    "          Positioned(\n" +
    "     left: {%=viewInfo.x %}.r,\n" +
    "     top: {%=viewInfo.y %}.r,\n" +
    "     child: Container(\n" +
    "      {% if (viewInfo.borderColor || viewInfo.border_width || viewInfo.border_radius) { %}\n" +
    "       decoration: BoxDecoration(\n" +
    "       {% if (viewInfo.border_radius) { %}borderRadius: const BorderRadius.all( Radius.circular({%=viewInfo.border_radius %})).r,{% } %}\n" +
    "           {% if (viewInfo.bgColor) { %}color: Color({%=viewInfo.bgColor %}),{% } %}\n" +
    "           {% if (viewInfo.box_shadow_color) { %}\n" +
    "               boxShadow: [\n" +
    "                    BoxShadow(\n" +
    "                       color: const Color({%=viewInfo.box_shadow_color %}),\n" +
    "                       offset: Offset({%=viewInfo.h_shadow %}.r, {%=viewInfo.v_shadow %}.r),	\n" +
    "                       blurRadius: {%=viewInfo.blur %}.r,\n" +
    "                       spreadRadius: {%=viewInfo.spread %}.r,\n" +
    "                      ),\n" +
    "               ],\n" +
    "           {% } %}\n" +
    "           {% if (viewInfo.borderColor || viewInfo.border_width) { %}\n" +
    "               border: Border.all(\n" +
    "           {% if (viewInfo.border_width) { %} width: {%=viewInfo.border_width %}.r,{% } %}\n" +
    "           {% if (viewInfo.borderColor) { %} color: Color({%=viewInfo.borderColor %}),{% } %}\n" +
    "               ),\n" +
    "       {% } %}\n" +
    "       ),\n" +
    "      {% } else { %}\n" +
    "      {% if (viewInfo.bgColor) { %}color: Color({%=viewInfo.bgColor %}),{% } %}\n" +
    "      {% } %}\n" +
    "        width: {%=viewInfo.width %}.r,\n" +
    "        height: {%=viewInfo.height %}.r,\n" +
    "        child: TextField(\n" +
    "         textAlign: TextAlign.start,\n" +
    "         decoration: InputDecoration(\n" +
    "          {% if (viewInfo.text) { %} hintText: {%=viewInfo.text %}, {% } %}\n" +
    "        hintStyle: TextStyle(color: Colors.grey),\n" +
    "        hintMaxLines: 1\n" +
    "         ),\n" +
    "         style: TextStyle(\n" +
    "        {% if (viewInfo.text) { %} fontFamily: \"{%=viewInfo.font_family %}\", {% } %}\n" +
    "        fontSize: {% if (viewInfo.font) { %}{%=viewInfo.font %}.sp{% } else { %}14.sp{% } %},\n" +
    "        color: Color({%=viewInfo.textColor %}),\n" +
    "        ),\n" +
    "      )\n" +
    "       )\n" +
    "    ),\n" +
    "          {% } %}\n" +
    "          {% if (viewInfo.viewType == \"image\") { %}\n" +
    "          Positioned(\n" +
    "     left: {%=viewInfo.x %}.r,\n" +
    "     top: {%=viewInfo.y %}.r,\n" +
    "     child: Container(\n" +
    "      {% if (viewInfo.borderColor || viewInfo.border_width || viewInfo.border_radius) { %}\n" +
    "       decoration: BoxDecoration(\n" +
    "       {% if (viewInfo.border_radius) { %}borderRadius: const BorderRadius.all( Radius.circular({%=viewInfo.border_radius %})).r,{% } %}\n" +
    "           {% if (viewInfo.bgColor) { %}color: Color({%=viewInfo.bgColor %}),{% } %}\n" +
    "           {% if (viewInfo.box_shadow_color) { %}\n" +
    "               boxShadow: [\n" +
    "                    BoxShadow(\n" +
    "                       color: const Color({%=viewInfo.box_shadow_color %}),\n" +
    "                       offset: Offset({%=viewInfo.h_shadow %}.r, {%=viewInfo.v_shadow %}.r),	\n" +
    "                       blurRadius: {%=viewInfo.blur %}.r,\n" +
    "                       spreadRadius: {%=viewInfo.spread %}.r,\n" +
    "                      ),\n" +
    "               ],\n" +
    "           {% } %}\n" +
    "           {% if (viewInfo.borderColor || viewInfo.border_width) { %}\n" +
    "               border: Border.all(\n" +
    "               {% if (viewInfo.border_width) { %} width: {%=viewInfo.border_width %}.r,{% } %}\n" +
    "               {% if (viewInfo.borderColor) { %} color: Color({%=viewInfo.borderColor %}),{% } %}\n" +
    "               ),\n" +
    "           {% } %}\n" +
    "       ),\n" +
    "      {% } else { %}\n" +
    "      {% if (viewInfo.bgColor) { %}color: Color({%=viewInfo.bgColor %}),{% } %}\n" +
    "      {% } %}\n" +
    "        width: {% if (viewInfo.width == maxW) { %}1.sw{% } else { %}{%=viewInfo.width %}.r{% } %},\n" +
    "        height: {% if (viewInfo.height == maxH) { %}1.sh{% } else { %}{%=viewInfo.height %}.r{% } %},\n" +
    "        child: Image.asset(\n" +
    "         {% if (viewInfo.image) { %}Assets.assets_images_{%=viewInfo.image %}_png,{% } %}\n" +
    "         fit: BoxFit.fill\n" +
    "      )\n" +
    "       )\n" +
    "    ),\n" +
    "          {% } %}\n" +
    "          {% if (viewInfo.viewType == \"button\") { %}\n" +
    "          Positioned(\n" +
    "     left: {%=viewInfo.x %}.r,\n" +
    "     top: {%=viewInfo.y %}.r,\n" +
    "     child: Container(\n" +
    "      {% if (viewInfo.borderColor || viewInfo.border_width || viewInfo.border_radius) { %}\n" +
    "       decoration: BoxDecoration(\n" +
    "       {% if (viewInfo.border_radius) { %}borderRadius: const BorderRadius.all( Radius.circular({%=viewInfo.border_radius %})).r,{% } %}\n" +
    "           {% if (viewInfo.bgColor) { %}color: Color({%=viewInfo.bgColor %}),{% } %}\n" +
    "           {% if (viewInfo.box_shadow_color) { %}\n" +
    "               boxShadow: [\n" +
    "                    BoxShadow(\n" +
    "                       color: const Color({%=viewInfo.box_shadow_color %}),\n" +
    "                       offset: Offset({%=viewInfo.h_shadow %}.r, {%=viewInfo.v_shadow %}.r),\n	" +
    "                       blurRadius: {%=viewInfo.blur %}.r,\n" +
    "                       spreadRadius: {%=viewInfo.spread %}.r,\n" +
    "                      ),\n" +
    "               ],\n" +
    "           {% } %}\n" +
    "           {% if (viewInfo.borderColor || viewInfo.border_width) { %}\n" +
    "               border: Border.all(\n" +
    "               {% if (viewInfo.border_width) { %} width: {%=viewInfo.border_width %}.r,{% } %}\n" +
    "               {% if (viewInfo.borderColor) { %} color: Color({%=viewInfo.borderColor %}),{% } %}\n" +
    "               ),\n" +
    "           {% } %}\n" +
    "       ),\n" +
    "      {% } else { %}\n" +
    "      {% if (viewInfo.bgColor) { %}color: Color({%=viewInfo.bgColor %}),{% } %}\n" +
    "      {% } %}\n" +
    "        width: {%=viewInfo.width %}.r,\n" +
    "        height: {%=viewInfo.height %}.r,\n" +
    "        child: SizedBox(\n" +
    "       child: TextButton(\n" +
    "        child: Text(\n" +
    "         {% if (viewInfo.text) { %} {%=viewInfo.text %} {% } %},\n" +
    "         textAlign: TextAlign.{%=viewInfo.align %},\n" +
    "         overflow: TextOverflow.visible,\n" +
    "         softWrap: false,\n" +
    "         style: TextStyle(\n" +
    "          {% if (viewInfo.text) { %} fontFamily: \"{%=viewInfo.font_family %}\", {% } %}\n" +
    "          fontSize: {% if (viewInfo.font) { %}{%=viewInfo.font %}.sp{% } else { %}14.sp{% } %},\n" +
    "          color: Color({%=viewInfo.textColor %}),\n" +
    "         ),\n" +
    "        ),\n" +
    "        onPressed: (){\n" +
    "         print(\"点击事件\");\n" +
    "        },\n" +
    "       )\n" +
    "        )\n" +
    "       )\n" +
    "    ),\n" +
    "          {% } %}\n" +
    "          {% if (viewInfo.viewType == \"container\") { %}\n" +
    "          Positioned(\n" +
    "     left: {%=viewInfo.x %}.r,\n" +
    "     top: {%=viewInfo.y %}.r,\n" +
    "     child: Container(\n" +
    "      {% if (viewInfo.borderColor || viewInfo.border_width || viewInfo.border_radius) { %}\n" +
    "       decoration: BoxDecoration(\n" +
    "       {% if (viewInfo.border_radius) { %}borderRadius: const BorderRadius.all( Radius.circular({%=viewInfo.border_radius %})).r,{% } %}\n" +
    "           {% if (viewInfo.bgColor) { %}color:const Color({%=viewInfo.bgColor %}),{% } %}\n" +
    "           {% if (viewInfo.box_shadow_color) { %}\n" +
    "               boxShadow: [\n" +
    "                    BoxShadow(\n" +
    "                       color: const Color({%=viewInfo.box_shadow_color %}),\n" +
    "                       offset: Offset({%=viewInfo.h_shadow %}.r, {%=viewInfo.v_shadow %}.r),	\n" +
    "                       blurRadius: {%=viewInfo.blur %}.r,\n" +
    "                       spreadRadius: {%=viewInfo.spread %}.r,\n" +
    "                      ),\n" +
    "               ],\n" +
    "           {% } %}\n"+
    "           {% if (viewInfo.borderColor || viewInfo.border_width) { %}\n" +
    "               border: Border.all(\n" +
    "               {% if (viewInfo.border_width) { %} width: {%=viewInfo.border_width %}.r,{% } %}\n" +
    "               {% if (viewInfo.borderColor) { %} color: Color({%=viewInfo.borderColor %}),{% } %}\n" +
    "               ),\n" +
    "           {% } %}\n" +
    "        ),\n" +
    "      {% } else { %}\n" +
    "      {% if (viewInfo.bgColor) { %}color: const Color({%=viewInfo.bgColor %}),{% } %}\n" +
    "      {% } %}\n" +
    "        width: {% if (viewInfo.width == maxW) { %}1.sw{% } else { %}{%=viewInfo.width %}.r{% } %},\n" +
    "        height: {% if (viewInfo.height == maxH) { %}1.sh{% } else { %}{%=viewInfo.height %}.r{% } %},\n" +
    "        {% if (viewInfo.views) { %}\n" +
    "                      {% var templateViewsValue = tmpl_flutter_views();var map = {};map[\"root\"] = viewInfo.views; var ret = tmpl(templateViewsValue, map); %}\n" +
    "                      {% if(ret) { %}\n" +
    "                      child: Stack(\n" +
    "         fit: StackFit.passthrough,\n" +
    "         children: <Widget>[\n" +
    "         {%=ret%}\n" +
    "         ]\n" +
    "         ),\n" +
    "         {% } %}\n" +
    "         {% } %}\n" +
    "       )\n" +
    "    ),\n" +
    "          {% } %}\n" +
    "\n" +
    "         {% } %}";
    return string;
}

//------------------- flutter模板 模块 -------------------



