# JyLanHuExportPlugIn
 JY蓝湖谷歌插件
 
谷歌插件之蓝湖代码生成器,iOS,Android,Swift,Flutter

这是一个谷歌插件,当打开蓝湖网站时,可识别出蓝湖生成的html代码并显示插件面板,可生成iOS,Android,Swift,Flutter代码,提取硬编码字符串，android圆角背景文件。

使用截图![使用GIF](/record.gif)

![截图](/dir_ic.png)

如何安装谷歌插件
https://www.somode.com/softjc/26336.html

现在开始流行Figma,MasterGo,那么这个插件该如何使用呢?
先将figma设计稿导入mastergo, 然后再将mastergo设计稿转化为蓝湖页面，再使用本插件。

如果帮到你，请我喝一杯咖啡，thanks
If it helps you, please buy me a cup of coffee.


微信支付                            
![微信支付](/wx_pay_cut.png)


支付宝支付    
![支付宝支付](/zhifu_pay_cut.png)
